<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 02/12/2017
 * Time: 11:27
 */

namespace Noa\POC;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Noa\POC\Annotation\IAnnotation;
use Noa\POC\Annotation\Optional;

abstract class EntityData
{
    protected $reader;

    public function __construct()
    {

        AnnotationRegistry::registerLoader('class_exists');

        $this->reader = new AnnotationReader();
    }

    public function __call($method, $args) {

        $refMethod = new \ReflectionMethod($this, $method);

        /**
         * @var IAnnotation $annotation
         */
        $annotation = $this->reader->getMethodAnnotation($refMethod, Optional::class);

        $value = $annotation->run($args[0]);

        if ($value !== Optional::KEY_NOT_FOUND) {

            call_user_func_array([$this, $method], [$value]);
        }

    }
}