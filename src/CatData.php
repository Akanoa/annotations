<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 02/12/2017
 * Time: 11:31
 */

namespace Noa\POC;
use Noa\POC\Annotation\Optional;


/**
 * Class CatData
 * @package Noa\POC
 */
class CatData extends EntityData
{
    /**
     * @var int
     * @property
     */
    public $speed = 0;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     * @return CatData
     * @Optional("speed")
     */
    protected function setSpeed(int $speed): CatData
    {

        $this->speed = $speed;
        return $this;
    }





}