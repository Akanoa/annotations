<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 02/12/2017
 * Time: 17:04
 */

namespace Noa\POC\Annotation;


interface IAnnotation
{
    public function run($data);
}