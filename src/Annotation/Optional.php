<?php
/**
 * Created by PhpStorm.
 * User: Noa
 * Date: 02/12/2017
 * Time: 15:44
 */

namespace Noa\POC\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Optional implements  IAnnotation {

    const KEY_NOT_FOUND = "_optional_annotation_key_not_found";

    private $key = '';

    public function __construct($options) {

        $this->key = $options['value'] ?: '';
    }

    public function run($data) {

        if (isset($data[$this->key])) {
            return $data[$this->key];
        }

        return self::KEY_NOT_FOUND;
    }
}